#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <string>
#include "potts.h"
#include "utilityfunctions.h"


int main(int argc, char **argv) {

    std::string filename;

    if(argc != 2){
        std::cout << "No Configuration File provided" << std::endl;
        exit(1);
    } else {
        filename = argv[1];
    }

    unsigned int dim_q;
    unsigned int dim_grid;

    if ( read_input(filename,&dim_q,&dim_grid) != 0 ){
        std::cout << "Error parsing input" << std::endl;
    }

    if ( dim_q == 2){
        std::cout << "Potts q="<<dim_q<<" (Ising) Model on a "<<dim_grid<<"x"<<dim_grid<<" lattice" << std::endl;
    } else {
        std::cout << "Potts q="<<dim_q<<" Model on a "<<dim_grid<<"x"<<dim_grid<<" lattice" << std::endl;
    }


    return(0);
}