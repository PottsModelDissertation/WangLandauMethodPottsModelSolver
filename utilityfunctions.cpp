#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <string>
#include "potts.h"
#include "utilityfunctions.h"

int read_input(std::string file,unsigned int *dim_q, unsigned int *dim_grid){
    std::fstream FILE;
    char line[256];
    FILE.open(file,std::ifstream::in);
    /* I'm lazy so I will run get line for each value that I know is in the cfg and expand later */

    FILE.getline(line,256);
    *dim_q = (unsigned int)std::strtoul(line,NULL,10);

    FILE.getline(line,256);
    *dim_grid = (unsigned int)std::strtoul(line,NULL,10);

    FILE.close();
    return(0);
}
