#ifndef POTTS_H
#define POTTS_H

class POTTS_MODEL{

    public:
        POTTS_MODEL();
        ~POTTS_MODEL();

    private:
        unsigned int q;
        unsigned int **grid;

};

#endif